# Weather Station

## Project Description
This is an Bluetooth based weather station, that to collect weather based data and sends it over BLE. The sensors used collects the following data:
- Temperature
- Humidity
- Wind direction
- Wind Speed
- Rain guage


## Goals
The main goal and reasoning for this project was to learn how to use the nRF Connect SDK, which should also develop Zephyr knowledge. In addition, this project should develop skills focusing on the full CICD, such as unit testing and static analysis..

- [ ] Follow CICD practices throughout the project
- [ ] Develop a reusable setup that can be used in other projects
- [ ] Create a PCB

## Roadmap
- [ ] Identify hardware components
- [ ] Purchase components
- [ ] Initialise basic project structure
- [ ] Develop project using CICD

## Hardware
- [ ] nRF52 (TBD)
- [ ] [Weather station](https://thepihut.com/products/wind-and-rain-sensors-for-weather-station-wind-vane-anemometer-rain-gauge?variant=42010298319043)
- [ ]  [BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/#:~:text=The%20BME280%20is%20a%20humidity,stability%20and%20high%20EMC%20robustness.) 

## Software
